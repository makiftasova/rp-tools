/**
 * roll.h - Dice roller of project
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * @date 21.01.2013
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/**
 * Return value of parser_dice function when success.
 */
#define PARSE_SUCCESS 5

/**
 * Minimum possible result of rolling dice.
 */
#define MINUMUM_RESULT 1

/**
 * Basic dice roller function
 * 
 * @param times How many times dice will be rolled
 * 
 * @param dice Which dice will be rolled (3, 6, 20, etc.)
 * 
 * @return Total of rolls
 */
int roll_dice(int times, int dice);

/**
 * Dice string parser function. Requires a string like "3d20+9" or "1d6-3"
 * 
 * @param string Dice string to parse (something like "3d20+8" )
 * 
 * @param times Times of roll 
 * 
 * @param dice Dice to roll
 * 
 * @param mod Modifier value
 * 
 * @param sign Sign of modifier (only "-" or "+" are acceptable)
 * 
 * @return 5 if success
 */
int parse_dice(const char* string, int* times, int* dice, char* sign, int* mod);

/**
 * Takes total value of rolled dices, sign of modifier and value of modifier 
 * then calculates roll's result. Only accepts "+" and "-" as sign of modifier.
 * 
 * @param dice Value of rolled dices
 * 
 * @param sign Sign of modifier
 * 
 * @param modifier Value of modifier
 * 
 * @return Roll's result. -1 if given sign is illegal
 */
int calc_roll_result(const int dice, const char sign, const int modifier);
