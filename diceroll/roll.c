/**
 * roll.c - Dice roller of project
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * @date 21.01.2013
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdio.h>
#include <stdlib.h>

#include "roll.h"
#include "random.h"

int roll_dice(const int times, const int dice)
{

	int* rolls = (int*) malloc(sizeof(int) * times);
	int i = 0;
	int total = 0;

	/* roll them */
	for (i = 0; i < times; ++i) {
		rolls[i] = (random1(dice) + 1);
	}

	/* calculate total */
	for (i = 0; i < times; ++i)
		total += rolls[i];

#ifdef DEBUG
	/* print all rolls to stdout if compiled in DEBUG mode*/
	for (i = 0; i < times; ++i)
		printf("%d -- %d\n", i + 1, rolls[i]);
	printf("total: %d\n", total);
#endif

	free(rolls);

	return total;
}

int parse_dice(const char* string, int* times, int* dice, char* sign, int* mod)
{
	char letter_d;
	return sscanf(string, "%d%c%d%c%d", times, &letter_d, dice, sign, mod);
}

int calc_roll_result(const int dice, const char sign, const int modifier)
{
	int result = -1;

	if (sign == '-')
		result = (dice - modifier);
	else if (sign == '+')
		result = (dice + modifier);

	result = (result < MINUMUM_RESULT) ? MINUMUM_RESULT : result;

	return result;
}
