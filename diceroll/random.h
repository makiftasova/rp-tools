/**
 * random.h - Random number generator of project
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * @date 21.01.2013
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <time.h>

/**
 * A Random integer generator which takes only upper limit.
 * Returns a random integer from range(0, maximum) (except maximum value)
 * 
 * @param maximum The upper limit of numbers
 * 
 * @return a pseudorandom number
 */
int random1(const int maximum);

/**
 * Only change this if you know what are you doing.
 */
#define RAND_SEED time(NULL)
