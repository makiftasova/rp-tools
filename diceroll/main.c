/**
 * main.c - main file of project
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * @date 21.01.2013
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "roll.h"
#include "random.h"

int main(int argc, char** argv)
{
	srand(RAND_SEED);
	int time = 0, dice = 0, mod = 0;
	char sign = '+';
	char* dice_string = (char*) malloc(11 * sizeof(char));

	/* loop counters*/
	unsigned int i = 0;

	if (argc == 1) {
		printf("Enter Dice String ==> ");
		scanf("%s", dice_string);
		parse_dice(dice_string, &time, &dice, &sign, &mod);
		printf("Roll: %d\n",
			calc_roll_result(roll_dice(time, dice), sign, mod));

		free(dice_string);

	} else if (argc > 1) {
		free(dice_string);
		for (i = 0; i < (argc - 1); ++i) {
			dice_string = argv[i + 1];
			parse_dice(dice_string, &time, &dice, &sign, &mod);
			printf("%d.Roll: %d\n", (i + 1),
				calc_roll_result(roll_dice(time, dice),
				sign, mod));
		}
	}





	return 0;
}
